﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelCreator : MonoBehaviour
{
    public int columnas;
    public int filas;
    public static Celda[,] laberinto;
    public Celda actual;
    public List<Celda> solucion = new List<Celda>();
    public List<Celda> backTrackList = new List<Celda>();

    public float tamañoCelula;

    public class Celda
    {
        public bool visitada = false;
        public int columna;
        public int fila;

        public GameObject celula;

        public GameObject suelo;

        public GameObject paredN;
        public GameObject paredE;
        public GameObject paredS;
        public GameObject paredO;

        bool[] paredes = { true, true, true, true };
        private int columnas;
        private int filas;
        //Abrir muro es la función que abre o quita paredes de las células basándose en columna (positiva abre pared este y negativa, oeste) y fila (positiva, norte; negativa sur)
        //              NORTE
        //              |f=1|
        //      OESTE               ESTE
        //      |c=-1|             |c=1|
        //                SUR
        //              |f=-1|
        public void abrirMuro(int c, int f)
        {
            if (c == 1)
            {
                paredE.SetActive(false);
            }
            
            if (c == -1)
            {
                paredO.SetActive(false);
            }
            if (f == -1)
            {
                paredS.SetActive(false);
            }
            if (f == 1)
            {
                paredN.SetActive(false);
            }

        }

        public List<Celda> buscarVecinos()
        {

            List<Celda> vecinos= new List<Celda>();

            //Vecino Norte
            if (fila > 0)
            {
                if (laberinto[columna, fila - 1].visitada == false)
                {

                    vecinos.Add(laberinto[columna, fila - 1]);
                    
                }
            }

            //Vecino Este
            if (columna < columnas-1)
            {
                if (laberinto[columna + 1, fila].visitada == false)
                {
                    vecinos.Add(laberinto[columna + 1, fila]);
                    
                }
            }

            //Vecino Sur
            if (fila < filas-1)
            {
                if (laberinto[columna, fila + 1].visitada == false)
                {
                    vecinos.Add(laberinto[columna, fila + 1]);
                    
                }
            }

            //Vecino Oeste
            if (columna > 0)
            {
                if (laberinto[columna - 1, fila].visitada == false)
                {
                    vecinos.Add(laberinto[columna - 1, fila]);
                    
                }

            }

            return vecinos;
        }
        //Constructor de Celda
        public Celda(int i, int j, float s, int c, int f)
        {
            //Parámetros de Celda
            columna = i;
            fila = j;
            columnas = c;
            filas = f;
            
            //Creación de Celda e instanciación de los GameObjects que la forman
            celula = new GameObject();
            celula.transform.position = new Vector3(i * s, 0, j * s);
            celula.name = "Celda " + i + "," + j;

            suelo = GameObject.CreatePrimitive(PrimitiveType.Plane);
            suelo.transform.SetParent(celula.transform);
            suelo.transform.localPosition = new Vector3(0, 0, 0);
            suelo.transform.localScale = new Vector3(s/10, 1, s/10);
            suelo.name = "Suelo";

            paredN = GameObject.CreatePrimitive(PrimitiveType.Cube);
            paredN.transform.SetParent(celula.transform);
            paredN.transform.localScale = new Vector3(s, 1, 1);
            paredN.transform.localPosition = new Vector3(0, .5f, s / 2);
            paredN.name = "Pared Norte";

            paredE = GameObject.CreatePrimitive(PrimitiveType.Cube);
            paredE.transform.SetParent(celula.transform);
            paredE.transform.localScale = new Vector3(1, 1, s);
            paredE.transform.localPosition = new Vector3(s/2, .5f, 0);
            paredE.name = "Pared Este";

            paredS = GameObject.CreatePrimitive(PrimitiveType.Cube);
            paredS.transform.SetParent(celula.transform);
            paredS.transform.localScale = new Vector3(s, 1, 1);
            paredS.transform.localPosition = new Vector3(0, .5f, -s / 2);
            paredS.name = "Pared Sur";

            paredO = GameObject.CreatePrimitive(PrimitiveType.Cube);
            paredO.transform.SetParent(celula.transform);
            paredO.transform.localPosition = new Vector3(-s / 2, .5f, 0);
            paredO.transform.localScale = new Vector3(1, 1, s);
            paredO.name = "Pared Oeste";

        }

    }

    // Start is called before the first frame update
    void Start()
    {
        
        //Iniciación del array bidimensional laberinto, que contiene todas las celdas del laberinto.
        laberinto = new Celda[columnas, filas];

        for(int i =0; i < columnas; i++)
        {
            for (int j = 0; j < filas; j++)
            {
                
                laberinto[i,j]=new Celda(i, j, tamañoCelula, columnas, filas) as Celda;

            }
        }

        //Asigna un valor a la célula actual para empezar el bucle de creación del laberinto (arbitrariamente Celda 0,0 aunque podría ser cualquiera)
        actual = laberinto[0, 0];
        int nVecinos = actual.buscarVecinos().Count;
        int random = Random.Range(0, nVecinos);
        Celda siguiente = actual.buscarVecinos()[random];
        actual.visitada = true;
        siguiente.visitada = true;
        backTrackList.Add(actual);
        actual.abrirMuro(-actual.columna + siguiente.columna, -actual.fila + siguiente.fila);
        siguiente.abrirMuro(-siguiente.columna + actual.columna, -siguiente.fila + actual.fila);
        actual = siguiente;
        //Bucle de creación del laberinto, recorre todo el laberinto introduciendo las células visitadas en el backtrack list, y quitándolas cuando ya no tienen más vecinos.
        while (backTrackList.Count>0)
        {
            nVecinos = actual.buscarVecinos().Count;
            random = Random.Range(0, nVecinos);
            //print(nVecinos + " " + random);

            if (nVecinos > 0)
            {
                siguiente = actual.buscarVecinos()[random];
                //print("actual " + actual.columna + "." + actual.fila + " siguiente " + siguiente.columna + "." + siguiente.fila+"");
                siguiente.visitada = true;
                backTrackList.Add(actual);
                //Si la celda actual es la celda de llegada, el backtrack list contendrá la solución del laberinto (arbitrariamente la celda de llegada es la ultima celda del laberinto)

                if (siguiente == laberinto[columnas-1, filas-1])
                {
                    //print("backTrackList " + backTrackList.Count);
                    for (int i=0; i<backTrackList.Count; i++)
                    {
                        solucion.Add(backTrackList[i]);
                    }
                    solucion.Add(siguiente);
                    print("solucion " + solucion.Count);
                    
                }
                actual.abrirMuro(-actual.columna + siguiente.columna, -actual.fila + siguiente.fila);
                siguiente.abrirMuro(-siguiente.columna + actual.columna, -siguiente.fila + actual.fila);
                actual = siguiente;
            
            }
            else
            {
                actual = backTrackList[backTrackList.Count - 1];
                backTrackList.RemoveAt(backTrackList.Count - 1);
                //print("RETROCESO");
            }
        }
        print("solucion " + solucion.Count);
        float increase = 1.0f/solucion.Count;
        for(int i = 0; i < solucion.Count; i++)
        {
            solucion[i].suelo.GetComponent<MeshRenderer>().material.color = new Color(1, 1 - i * increase, 1 - i * increase, 1);

        }

    }

    // Update is called once per frame
    void Update()
    {
        //Descomentar este código y comentar el equivalente en Start() para ver cómo el laberinto se forma en Update.
        /*
        int nVecinos = actual.buscarVecinos().Count;
        int random = Random.Range(0, nVecinos);
        
        print(nVecinos + " " + random);
        if (nVecinos > 0)
        {
            Celda siguiente = actual.buscarVecinos()[random];
            print("actual " + actual.columna + "." + actual.fila + " siguiente " + siguiente.columna + "." + siguiente.fila+"");
            siguiente.visitada = true;
            backTrackList.Add(actual);
            actual.abrirMuro(-actual.columna + siguiente.columna, -actual.fila + siguiente.fila);
            siguiente.abrirMuro(-siguiente.columna + actual.columna, -siguiente.fila + actual.fila);
            actual = siguiente;
            
        }
        else
        {
            actual = backTrackList[backTrackList.Count - 1];
            backTrackList.RemoveAt(backTrackList.Count - 1);
            print("RETROCESO");
        }*/
    }
}



